const axios = require('axios');
var fs = require('fs');
 
axios.get('https://ip-ranges.amazonaws.com/ip-ranges.json')
  .then(function (response) {
    const cidrs = response.data.prefixes.map(c => c.ip_prefix);
    let file = fs.createWriteStream('cidrs.txt');
    file.on('error', function(err) { console.error(err) });
    cidrs.forEach(element => {
        file.write(`${element}\n`)
    });
  })
  .catch(function (error) { console.log(error); })
